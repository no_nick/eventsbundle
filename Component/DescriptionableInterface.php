<?php
/**
 * Created by PhpStorm.
 * User: aterepora
 * Date: 07.01.17
 * Time: 13:07
 */

namespace AT\EventsBundle\Component;


interface DescriptionableInterface
{
    /**
     * @param string $description
     * @return self
     */
    public function setDescription($description);

    /**
     * @return string|null
     */
    public function getDescription();
}