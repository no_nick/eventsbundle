<?php
/**
 * Created by PhpStorm.
 * User: aterepora
 * Date: 20.12.16
 * Time: 21:49
 */

namespace AT\EventsBundle\Component;


interface NameableInterface
{
    /**
     * Returns the name name
     *
     * @return string
     */
    public function getName();

    /**
     * Sets the name
     *
     * @param string $name
     * @return self
     */
    public function setName($name);
}