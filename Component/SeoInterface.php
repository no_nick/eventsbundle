<?php
/**
 * Created by PhpStorm.
 * User: aterepora
 * Date: 02.01.17
 * Time: 22:36
 */

namespace AT\EventsBundle\Component;


interface SeoInterface
{
    /**
     * @param string $metaKeywords
     * @return self
     */
    public function setMetaKeywords($metaKeywords);

    /**
     * @return string|null
     */
    public function getMetaKeywords();

    /**
     * @param string $metaDescription
     * @return self
     */
    public function setMetaDescription($metaDescription);

    /**
     * @return string|null
     */
    public function getMetaDescription();
}