<?php
/**
 * Created by PhpStorm.
 * User: aterepora
 * Date: 02.01.17
 * Time: 22:23
 */

namespace AT\EventsBundle\Component;


interface SlugableInterface
{
    /**
     * @return string|null
     */
    public function getSlug();

    /**
     * @param string $slug
     * @return self
     */
    public function setSlug($slug = null);
}