<?php
/**
 * Created by PhpStorm.
 * User: aterepora
 * Date: 02.01.17
 * Time: 21:57
 */

namespace AT\EventsBundle\Component;


interface ResourceInterface
{
    /**
     * @return mixed
     */
    public function getId();
}