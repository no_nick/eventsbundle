<?php
/**
 * Created by PhpStorm.
 * User: aterepora
 * Date: 02.01.17
 * Time: 22:19
 */

namespace AT\EventsBundle\Component;


interface ToggleableInterface
{
    /**
     * @param bool $active
     * @return self
     */
    public function setActive($active);

    /**
     * @return bool
     */
    public function isActive();

    public function activate();

    public function deactivate();
}