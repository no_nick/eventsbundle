<?php
/**
 * Created by PhpStorm.
 * User: aterepora
 * Date: 02.01.17
 * Time: 21:54
 */

namespace AT\EventsBundle\Component;


interface TimestampableInterface
{
    /**
     * Get object creation datetime
     *
     * @return \DateTime|null
     */
    public function getCreatedAt();

    /**
     * Set object creation datetime
     *
     * @param \DateTime $createdAt
     * @return self
     */
    public function setCreatedAt(\DateTime $createdAt);

    /**
     * Get object update datetime
     *
     * @return \DateTime|null
     */
    public function getUpdatedAt();

    /**
     * Set object update datetime
     *
     * @param \DateTime $updatedAt
     * @return self
     */
    public function setUpdatedAt(\DateTime $updatedAt);
}