<?php

namespace AT\EventsBundle;

use AT\EventsBundle\Entity\Event;
use Symfony\Component\HttpKernel\Bundle\Bundle;
use Doctrine\Bundle\DoctrineBundle\DependencyInjection\Compiler\DoctrineOrmMappingsPass;
use Symfony\Component\DependencyInjection\ContainerBuilder;

class ATEventsBundle extends Bundle
{
    public function build(ContainerBuilder $container)
    {
        parent::build($container);

        $modelDir = realpath(__DIR__.'/Resources/config/doctrine');
        $mappings = array(
            $modelDir => Event::class,
        );

        if (class_exists(DoctrineOrmMappingsPass::class)) {
            $container->addCompilerPass(
                DoctrineOrmMappingsPass::createXmlMappingDriver(
                    $mappings,
                    array('at_events.event_manager_name'),
                    'at_events.backend_type_orm',
                    array('ATEventsBundle' => Event::class)
                )
            );
        }
    }

}
