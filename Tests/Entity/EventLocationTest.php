<?php
/**
 * Created by PhpStorm.
 * User: aterepora
 * Date: 02.01.17
 * Time: 21:40
 */

namespace AT\EventsBundle\Tests\Entity;

use PhpSpec\Exception\Exception;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use AT\EventsBundle\Entity\EventLocation;

class EventLocationTest extends WebTestCase
{
    public function testEventLocationName()
    {
        $eventsLocation = $this->getEventLocation();
        $this->assertNull($eventsLocation->getName());
        $this->assertNull($eventsLocation->getCapacity());
        $this->assertNull($eventsLocation->getCreatedAt());
        $eventsLocation->setName('Hala Globus');
        $eventsLocation->setCapacity(5000);
        $eventsLocation->setCreatedAt(new \DateTime());
    }

    public function testFailedCapacity()
    {
        $eventsLocation = $this->getEventLocation();
        $this->expectException(Exception::class);
        $this->expectExceptionMessage('Capacity must be an integer.');
        $eventsLocation->setCapacity('abc');
    }

    public function testEventLocationDates()
    {
        $eventsLocation = $this->getEventLocation();
        $this->assertNull($eventsLocation->getCreatedAt());
        $this->assertNull($eventsLocation->getUpdatedAt());

        $eventsLocation->setCreatedAt(new \DateTime('2000-01-01'));
        $this->assertSame('2000-01-01 00:00:00', $eventsLocation->getCreatedAt()->format('Y-m-d H:i:s'));
        $eventsLocation->setUpdatedAt(new \DateTime('2001-02-03 04:05:06'));
        $this->assertSame('2001-02-03 04:05:06', $eventsLocation->getUpdatedAt()->format('Y-m-d H:i:s'));
        $this->assertEquals(new \DateTime('2001-02-03 04:05:06'), $eventsLocation->getUpdatedAt());
    }

    public function testActivation()
    {
        $eventsLocation = $this->getEventLocation();
        $this->assertFalse($eventsLocation->isActive());
        $eventsLocation->setActive(true);
        $this->assertTrue($eventsLocation->isActive());
        $eventsLocation->deactivate();
        $this->assertFalse($eventsLocation->isActive());
        $eventsLocation->activate();
        $this->assertTrue($eventsLocation->isActive());
    }

    public function testRelations()
    {
        $event = $this->getMockBuilder('AT\EventsBundle\Entity\Event')->getMock();
        $event->method('getName')->willReturn('Projekt testowy');

        $eventsLocation = $this->getEventLocation();
        $this->assertEmpty($eventsLocation->getEvents());
        $this->assertFalse($eventsLocation->hasEvent($event));
        $this->assertFalse($eventsLocation->hasEvents());
        $eventsLocation->addEvent($event);
        $this->assertInstanceOf('Doctrine\Common\Collections\ArrayCollection', $eventsLocation->getEvents());
        $this->assertNotEmpty($eventsLocation->getEvents());
        $this->assertTrue($eventsLocation->hasEvent($event));
        $this->assertTrue($eventsLocation->hasEvents());
        $eventsLocation->removeEvent($event);
        $this->assertEmpty($eventsLocation->getEvents());
        $this->assertFalse($eventsLocation->hasEvent($event));
        $this->assertFalse($eventsLocation->hasEvents());
    }

    /**
     * @return EventLocation
     */
    protected function getEventLocation()
    {
        return $this->getMockForAbstractClass('AT\EventsBundle\Entity\EventLocation');
    }
}