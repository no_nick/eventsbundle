<?php
/**
 * Created by PhpStorm.
 * User: aterepora
 * Date: 02.01.17
 * Time: 21:40
 */

namespace AT\EventsBundle\Tests\Entity;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use AT\EventsBundle\Entity\Event;

class EventTest extends WebTestCase
{
    public function testEventName()
    {
        $event = $this->getEvent();
        $this->assertNull($event->getName());

        $event->setName('Projekt X');
        $this->assertSame('Projekt X', $event->getName());
    }

    public function testEventsDates()
    {
        $event = $this->getEvent();
        $this->assertNull($event->getCreatedAt());
        $this->assertNull($event->getUpdatedAt());
        $this->assertNull($event->getStartedAt());
        $this->assertNull($event->getEndedAt());

        $event->setCreatedAt(new \DateTime('2000-01-01'));
        $this->assertSame('2000-01-01 00:00:00', $event->getCreatedAt()->format('Y-m-d H:i:s'));
        $event->setUpdatedAt(new \DateTime('2001-02-03 04:05:06'));
        $this->assertSame('2001-02-03 04:05:06', $event->getUpdatedAt()->format('Y-m-d H:i:s'));
        $this->assertEquals(new \DateTime('2001-02-03 04:05:06'), $event->getUpdatedAt());

        $event->setStartedAt(new \DateTime('2016-01-01 09:00:00'));
        $this->assertSame('2016-01-01 09:00:00', $event->getStartedAt()->format('Y-m-d H:i:s'));
        $event->setEndedAt(new \DateTime('2016-01-01 09:00:00'));
        $this->assertSame('2016-01-01 09:00:00', $event->getEndedAt()->format('Y-m-d H:i:s'));

        $this->assertFalse($event->isCurrentlyRealized());
        $event->setEndedAt(new \DateTime('tomorrow'));
        $this->assertTrue($event->isCurrentlyRealized());
    }

    public function testActivation()
    {
        $event = $this->getEvent();
        $this->assertFalse($event->isActive());

        $event->setActive(true);
        $this->assertTrue($event->isActive());

        $event->deactivate();
        $this->assertFalse($event->isActive());

        $event->activate();
        $this->assertTrue($event->isActive());
    }

    public function testRelations()
    {
        $eventLocationMock = $this->getMockBuilder('AT\EventsBundle\Entity\EventLocation')->getMock();
        $eventLocationMock
            ->method('getName')->willReturn('Hala Globus');

        $this->assertEquals('Hala Globus', $eventLocationMock->getName());

        $event = $this->getEvent();
        $this->assertEmpty($event->getEventLocation());
        $event->setEventLocation($eventLocationMock);
        $this->assertInstanceOf('AT\EventsBundle\Entity\EventLocationInterface', $event->getEventLocation());
        $this->assertEquals('Hala Globus', $event->getEventLocation()->getName());
    }

    /**
     * @return Event
     */
    protected function getEvent()
    {
        return $this->getMockForAbstractClass('AT\EventsBundle\Entity\Event');
    }
}