<?php
/**
 * Created by PhpStorm.
 * User: aterepora
 * Date: 07.01.17
 * Time: 13:12
 */

namespace AT\EventsBundle\Entity;


use AT\EventsBundle\Component\NameableInterface;
use AT\EventsBundle\Component\ResourceInterface;
use AT\EventsBundle\Component\TimestampableInterface;
use AT\EventsBundle\Component\ToggleableInterface;

interface EventLocationInterface extends
    ResourceInterface,
    TimestampableInterface,
    NameableInterface,
    ToggleableInterface
{
    /**
     * @param int $capacity
     * @return self
     */
    public function setCapacity($capacity);

    /**
     * @return int
     */
    public function getCapacity();
}