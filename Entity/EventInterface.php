<?php
/**
 * Created by PhpStorm.
 * User: aterepora
 * Date: 19.12.16
 * Time: 23:11
 */

namespace AT\EventsBundle\Entity;


use AT\EventsBundle\Component\NameableInterface;
use AT\EventsBundle\Component\DescriptionableInterface;
use AT\EventsBundle\Component\ResourceInterface;
use AT\EventsBundle\Component\SeoInterface;
use AT\EventsBundle\Component\TimestampableInterface;
use AT\EventsBundle\Component\ToggleableInterface;

interface EventInterface extends
    TimestampableInterface,
    ResourceInterface,
    NameableInterface,
    DescriptionableInterface,
    ToggleableInterface,
    SeoInterface
{
    /**
     * @return \DateTime|null;
     */
    public function getStartedAt();

    /**
     * @return \DateTime|null;
     */
    public function getEndedAt();

    /**
     * @param \DateTime $startedAt
     * @return self
     */
    public function setStartedAt(\DateTime $startedAt);

    /**
     * @param \DateTime $endedAt
     * @return self
     */
    public function setEndedAt(\DateTime $endedAt);

    /**
     * Returns information whether event is live now or not
     *
     * @return bool
     */
    public function isCurrentlyRealized();

    /**
     * @param EventLocationInterface $eventLocation
     * @return self
     */
    public function setEventLocation(EventLocationInterface $eventLocation);

    /**
     * @return EventLocationInterface|null
     */
    public function getEventLocation();
}