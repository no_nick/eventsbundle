<?php
/**
 * Created by PhpStorm.
 * User: aterepora
 * Date: 07.01.17
 * Time: 13:27
 */

namespace AT\EventsBundle\Entity;


interface EventableInterface
{
    /**
     * Gets the events assigned to object
     *
     * @return \Traversable
     */
    public function getEvents();

    /**
     * Add an Event to the object
     *
     * @param EventInterface $event
     * @return self
     */
    public function addEvent(EventInterface $event);

    /**
     * Remove the event from the events
     *
     * @param EventInterface $event
     * @return self
     */
    public function removeEvent(EventInterface $event);

    /**
     * Indicates whether event is assigned to the specified object or not
     *
     * @param EventInterface $event
     * @return bool
     */
    public function hasEvent(EventInterface $event);

    /**
     * Sets an collection of events
     *
     * @param array $events
     * @return self
     */
    public function setEvents(array $events);

    /**
     * Indicates whether any events are assigned or not
     *
     * @return bool
     */
    public function hasEvents();
}